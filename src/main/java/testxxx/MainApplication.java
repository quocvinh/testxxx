package testxxx;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MainApplication {

    public static JsonObject createObject() {
        JsonElement test = new JsonParser().parse("{\"_id\":\"5cef57a6d401c9e3d8ef9cc3\",\"index\":0,\"guid\":\"327a6586-6191-445e-9b21-e091bc02c4d2\",\"isActive\":false,\"balance\":\"$3,595.11\",\"picture\":\"http://placehold.it/32x32\",\"age\":24,\"eyeColor\":\"blue\",\"name\":\"Richardson Short\",\"gender\":\"male\",\"company\":\"SLAX\",\"email\":\"richardsonshort@slax.com\",\"phone\":\"+1 (809) 420-2546\",\"address\":\"594 Willoughby Avenue, Morriston, New Mexico, 5387\",\"about\":\"Occaecat in cupidatat fugiat reprehenderit mollit Lorem ullamco do cupidatat Lorem ea dolor ea. Incididunt commodo Lorem ad exercitation minim officia dolore voluptate nostrud. Do tempor nisi eiusmod adipisicing aliqua ea nisi consectetur occaecat et cupidatat. Dolore et nisi proident reprehenderit irure consectetur cupidatat. Quis fugiat excepteur ut labore ullamco eu. Cillum proident cupidatat deserunt irure consequat ea. Laboris eu cillum in amet est commodo et in sint ea magna eu minim enim.\\r\\n\",\"registered\":\"2018-08-18T01:37:47 -07:00\",\"latitude\":69.323288,\"longitude\":-80.529164,\"tags\":[\"cillum\",\"occaecat\",\"consectetur\",\"amet\",\"aute\",\"Lorem\",\"labore\"],\"friends\":[{\"id\":0,\"name\":\"Peterson Neal\"},{\"id\":1,\"name\":\"Mclean Moreno\"},{\"id\":2,\"name\":\"Head Ward\"}],\"greeting\":\"Hello, Richardson Short! You have 10 unread messages.\",\"favoriteFruit\":\"strawberry\"}");
        return test.getAsJsonObject();
    }

    public static void main(String[] args){
        JsonObject object = createObject();
        System.out.println(object.get("_id").getAsString());
        System.out.println(object.get("guid").getAsBoolean());
    }

}
